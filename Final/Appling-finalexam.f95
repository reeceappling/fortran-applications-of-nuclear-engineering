PROGRAM APPLING_FINALEXAM

	!Author: Reece Appling
	!Date: 04/30/2021
	!Assignment Name: Final Exam
	!Assignment Description: -----------------------------------------------------------------------------------------------------------

	IMPLICIT NONE
	
	CHARACTER(72)::caseTitle,shieldType
	CHARACTER(86),PARAMETER::line="--------------------------------------------------------------------------------------"
	CHARACTER(1),PARAMETER::v="|"
	INTEGER::nNuclides,i,timesteps !# nuclides, iteration, # timesteps
	REAL::rhom,A,rho,shieldrad,dt !shield rho, A, rho, radius, dt
	REAL,ALLOCATABLE::macroCrs(:),N(:),lambda(:),theta(:),R(:),dataOut(:,:)
	REAL,EXTERNAL::UncollidedDose
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		
	!---------------End Variable Declaration--------------!

	!Print header
	CALL HEADER()
	
	!Read in first set of data
	READ(*,'(A72)')caseTitle
	READ(*,*)!skip line
	READ(*,*)shieldType
	READ(*,*)shieldrad
	READ(*,*)dt
	READ(*,*)timesteps
	READ(*,*)nNuclides
	
	!Ensure shield type is valid
	SELECT CASE(shieldType)
		CASE("Water")
			rhom = 1.00
			A = 18.016
		CASE("Iron")
			rhom = 7.86
			A = 55.85
		CASE("Lead")
			rhom = 11.35
			A = 207.21
		CASE DEFAULT
		WRITE(*,*)"Error: input shield type was not Water, Iron, or Lead. Only those values are acceptable"
		STOP "Invalid Shield type"
	END SELECT
	!Calculate rho
	rho = 6.0211409*(10.0**23)*rhom/A
	
	
	!Output starting table
	WRITE(*,'(A72)')caseTitle
	WRITE(*,'(A60)')line
	WRITE(*,'(A1,2X,A30,T45,A1,9X,A5,T60,A1)')v,"Shielding Material",v,shieldType,v
	WRITE(*,'(A1,2X,A30,T45,A1,2X,F12.6,T60,A1)')v,"Shield mass density (gm/cm^3)",v,rhom,v
	WRITE(*,'(A1,2X,A30,T45,A1,2X,F12.6,T60,A1)')v,"Shield radius (cm)",v,shieldrad,v
	WRITE(*,'(A1,2X,A30,T45,A1,2X,E13.6,T60,A1)')v,"Time Step (s)",v,dt,v
	WRITE(*,'(A1,2X,A30,T45,A1,T56,I3,T60,A1)')v,"Number of radioisotopes in source",v,nNuclides,v
	WRITE(*,'(A60)')line
	
	!Before reading nuclides, ensure the number of them is correct
	IF(nNuclides<1) THEN
		WRITE(*,*)"Error: input specified less than 1 nuclide will be provided, executing termination"
		STOP "Invalid number of nuclides"
	END IF
	
	!Allocate arrays for isotope and system data
	ALLOCATE(macroCrs(nNuclides),N(nNuclides),lambda(nNuclides),theta(nNuclides),R(nNuclides))
	!Allocate array for output data
	ALLOCATE(dataOut(timesteps,2))
	
	!Read in all nuclide data
	DO i=1,nNuclides
		READ(*,*)!Skip isotope name line
		READ(*,*)N(i)
		READ(*,*)lambda(i)
		READ(*,*)theta(i)
		READ(*,*)R(i)
		!Calculate macroscopic cross section
		macroCrs(i)=rho*theta(i)*(10.0**(-24))
	END DO
	
	!Echo out radioIsotope Energy...
	WRITE(*,*)"Parameters dependent on radioisotope energy:"
	DO i=1,nNuclides
		WRITE(*,'(A14,10X,I2)')"*** Radisotope",i
		WRITE(*,'(T08,A,T40,E13.6)')"Initial atomic density (gm/cm^3)",N(i)
		WRITE(*,'(T08,A,T40,E13.6)')"Decay constant (s^-1)",lambda(i)
		WRITE(*,'(T08,A,T40,E13.6)')"Microscopic total cross-section (b)",theta(i)
		WRITE(*,'(T08,A,T40,E13.6)')"Response Function",R(i)
	END DO
	
	!Check data
	IF(shieldrad<=0) THEN
		WRITE(*,*)"Error: Shield radius must be positive, terminating"
		STOP
	END IF
	
	IF(dt<=0) THEN
		WRITE(*,*)"Error: dt must be positive, terminating"
		STOP
	END IF
	
	IF(timesteps<1) THEN
		WRITE(*,*)"Error: timesteps must be positive, terminating"
		STOP
	END IF
	
	!Check all nuclide data
	DO i=1,nNuclides
		IF(N(i)<=0) THEN
			WRITE(*,*)"Error: Density for each nuclide must be positive, terminating"
			STOP
		END IF
		IF(lambda(i)<=0) THEN
			WRITE(*,*)"Error: Decay constants for each nuclide must be positive, terminating"
			STOP
		END IF
		IF(theta(i)<=0) THEN
			WRITE(*,*)"Error: Microscopic crs for each nuclide must be positive, terminating"
			STOP
		END IF
		IF(R(i)<=0) THEN
			WRITE(*,*)"Error: response functions for each nuclide must be positive, terminating"
			STOP
		END IF
	END DO
	
	!If we made it this far, congratulate on proper input
	WRITE(*,'(A80,2/)')"Congratulations on entering proper input values. Code execution will now proceed"
	
	!Calculate all data
	DO i=1,timesteps
		dataOut(i,1)=dt*(i-1)
		dataOut(i,2)=UncollidedDose(nNuclides,N,lambda,dt*(i-1),macroCrs,shieldrad,R)
	END DO
	
	!Output all data in final table
	WRITE(*,'(A60,/)')line
	WRITE(*,'(A,T20,A,T40,A)')"Time step","Time (s)","Dose (Gy/s)"
	372 FORMAT (T03,I3,T20,E13.6,T40,E13.6)
	DO i=1,timesteps
		WRITE(*,372)i,dataOut(i,1),dataOut(i,2)
	END DO
END PROGRAM APPLING_FINALEXAM


REAL FUNCTION UncollidedDose(nI,No,lambda,t,macro,rad,response)
	IMPLICIT NONE
	INTEGER::nI
	REAL::No(nI),lambda(nI),macro(nI),response(nI)
	REAL::t,rad
	INTEGER::j
	REAL::sums,temp
	REAL,PARAMETER::e=2.718281828,pi=3.141592
	
	sums=0
	temp=0
	!Iterate over each isotope
	DO j=1,nI
		!Calculate out the term to be summed for this isotope
		temp=response(j)*lambda(j)*No(j)
		temp=temp*(e**(0-(t*lambda(j))))
		temp=temp/(4*pi*(rad**2))
		temp=temp*(e**(0-(rad*macro(j))))
		!Add this iteration to the sum
		sums=sums+temp
	END DO
	!Finally, set the function output to the sum
	UncollidedDose = sums
END FUNCTION


!Outputs header to screen
SUBROUTINE HEADER()
	IMPLICIT NONE
	WRITE(*,*)"Final Exam, programmed by Reece Appling on April 30th, 2021"
END SUBROUTINE HEADER