 Final Exam, programmed by Reece Appling on April 30th, 2021
Test Case For Final Exam                                                
------------------------------------------------------------
|              Shielding Material           |         Lead |
|   Shield mass density (gm/cm^3)           |     11.350000|
|              Shield radius (cm)           |      1.000000|
|                   Time Step (s)           |   0.100000E+0|
|  Number of radioisotopes in sou           |            3 |
------------------------------------------------------------
 Parameters dependent on radioisotope energy:
*** Radisotope           1
       Initial atomic density (gm/cm^3) 0.100000E+17
       Decay constant (s^-1)            0.212000E-04
       Microscopic total cross-section  0.240000E+03
       Response Function                0.122000E-11
*** Radisotope           2
       Initial atomic density (gm/cm^3) 0.100000E+14
       Decay constant (s^-1)            0.998000E-06
       Microscopic total cross-section  0.112200E+03
       Response Function                0.187000E-11
*** Radisotope           3
       Initial atomic density (gm/cm^3) 0.100000E+13
       Decay constant (s^-1)            0.693000E-08
       Microscopic total cross-section  0.363800E+02
       Response Function                0.340000E-11
Congratulations on entering proper input values. Code execution will now proceed


------------------------------------------------------------

Time step          Time (s)            Dose (Gy/s)
    1               0.000000E+00        0.755095E-05
    2               0.100000E+07        0.140892E-07
    3               0.200000E+07        0.554380E-08
    4               0.300000E+07        0.239140E-08
    5               0.400000E+07        0.122697E-08
    6               0.500000E+07        0.795359E-09
    7               0.600000E+07        0.633891E-09
    8               0.700000E+07        0.572018E-09
    9               0.800000E+07        0.546874E-09
   10               0.900000E+07        0.535285E-09
   11               0.100000E+08        0.528709E-09
   12               0.110000E+08        0.523996E-09
   13               0.120000E+08        0.519986E-09
   14               0.130000E+08        0.516251E-09
   15               0.140000E+08        0.512632E-09
   16               0.150000E+08        0.509072E-09
   17               0.160000E+08        0.505549E-09
   18               0.170000E+08        0.502055E-09
   19               0.180000E+08        0.498587E-09
   20               0.190000E+08        0.495144E-09
