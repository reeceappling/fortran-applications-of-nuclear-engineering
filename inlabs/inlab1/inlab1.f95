PROGRAM APPLING_INLAB1

	!Author: Reece Appling
	!Date: 1/29/2021
	!Assignment Name: InLab 1
	!Assignment Description: Calculates molecular density using mass number, mass density, and Avagadro's number.
	
	IMPLICIT NONE
	CHARACTER(30)::user_name
	CHARACTER(2)::element
	INTEGER::A
	REAL::rho,output
	REAL,PARAMETER::Na=6.022E+23
	
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!user_name - Holds user's name
		!element - 2-letter symbol for element
	!INTEGER VARIABLES
		!A - Holds mass number of element (g/cm3)
	!REAL VARIABLES
		!rho - Holds mass density of element
		!output - Holds molecular density of element
	!REAL PARAMETERS
		!Na - Avagadro's Number
	!---------------End Variable Declaration--------------!

	!Prompt user to enter their name
	WRITE(*,*) "Please enter your name (Max 30 characters): "
	READ(*,'(A30)') user_name !Read user input, store it as user_name

	!Write line for calculator made by, then executed by
	WRITE(*,*) "Molecular density calculator programmed by Reece Appling"
	WRITE(*,*) "Executed by ", user_name !Write line for calculator executed by

	!Prompt user to input 2-letter symbol for an element (eg. U or Na)
	WRITE(*,*) "Please input your element's atomic symbol (eg. U or Na): "
	READ(*,*) element !Read element symbol from user

	!Prompt user to input mass number of the element (eg. 238 or 23)
	WRITE(*,*) "Please input your element's mass number (eg. 238 or 23): "
	READ(*,*) A !Read mass number from user

	!Prompt user to input the mass density of their element in g/cm3 (eg. 18.95 or 0.97)
	WRITE(*,*) "Please input your element's mass density in g/cm3 (eg. 18.95 or 0.97): "
	READ(*,*) rho !Read mass density from user

	!Calculate molecular density in molecules/cm3
	output = Na*rho/A

	!Output with correct format
	WRITE(*,'(A,I3,2A,E14.6,A)') "Molecular density of ", A, element, " =", output, " per cm3"
END PROGRAM APPLING_INLAB1