PROGRAM APPLING_INLAB6

	!Author: Reece Appling
	!Date: 2/26/2021
	!Assignment Name: Inlab 6
	!Assignment Description: Given atomic density and decay constant,
	!	can calculate timestamped tables of atomic densities of 
	!	parent and child nuclide over user-defined time ranges

	IMPLICIT NONE
	
	CHARACTER(60),PARAMETER::line='============================================================'
	INTEGER::errors=0,n=1,i
	REAL::No,lambda,t1,tn,results(3),nReal
	REAL,PARAMETER::e=2.7182818
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders and dividers
	!INTEGER VARIABLES
		!errors - Holds number of detected input errors, starts as 0
		!n - Holds requested number of timesteps, starts at 1 because it will just be re-set before it is used
	!REAL VARIABLES
		!No - Holds initial atomic density of nuclide A
		!lambda - Holds decay constant for nuclide A (/s)
		!t1 - Holds starting time (s)
		!tn - Holds ending time (s)
		!results - Array of results with 3 entries
			!results(1) - time (s)
			!results(2) - Density of nuclide A at the time of results(1)
			!results(3) - Density of nuclide B at the time of results(1)
		!nReal - REAL equivalent of INTEGER n
	!REAL PARAMETERS
		!e - Euler's number
	!---------------End Variable Declaration--------------!
	
	!Displays Header Information
	WRITE(*,'(A)') line(:55)
	WRITE(*,'(A)') "This code was developed by Reece Appling on Feb 26th 2021"
	WRITE(*,'(A)') "The purpose of the code is to determine the atomic density"
	WRITE(*,'(A55)') line
	
	!Prompts user to enter initial atomic densityof A, then records it
	WRITE(*,'(/,A)') "Please enter the initial atomic density of A:"
	READ(*,*) No
	
	!Prompts user to enter decay constant, then records it
	WRITE(*,'(A)') "Please enter the decay constant:"
	READ(*,*) lambda
	
	!Display input data
	WRITE(*,'(/,A25,A10,A25)') line,"INPUT DATA",line
	WRITE(*,'(A,E13.6,A)') "Inititial Atomic Density of A = ",No," cm-3"
	WRITE(*,'(A,E13.6,A)') "Decay constant = ",lambda," s-1"
	WRITE(*,'(A60,/)') line !lower border of input data followed by space before error checks
	
	!Ensure decay const >0
	IF(lambda<=0) THEN
		errors=errors+1
		WRITE(*,*) "Error: lambda must be greater than 0"
	END IF
	
	!Ensure initial atomic density of A >= 0
	IF(No<0) THEN
		errors=errors+1
		WRITE(*,*) "Error: initial atomic density of A must not be negative"
	END IF
	
	!If errors exist, exit program, otherwise continue
	IF(errors>0) THEN !Display number of errors and exit program
		WRITE(*,'(A,I1)') "Errors detected: ",errors
		STOP ': Program terminated due to input errors'
	ELSE !Notify inputs valid, then do main run loop while n>0
		WRITE(*,*) "Congratulations! There are no input mistakes."
		DO WHILE(n>=0) !while the user selected value of n>0, continue looping
			!Ask user to input # of time steps, record, echo, set nReal
			WRITE(*,*) "Please enter the number of time steps"
			READ(*,*) n
			WRITE(*,'(A,I5)') "The number of time steps = ",n
			nReal = REAL(n) !Make nReal REAL value of n
			!If n<0 then notify and terminate
			IF(n<=0) THEN 
				WRITE(*,'(A,I5,A)') "The number of time steps: ",n," is not greater than 0!"
				WRITE(*,*) "Please press any key to terminate"
				READ(*,*)
				STOP
			ELSE !If n>0 then ask and record both starting and ending times
				WRITE(*,*) "Please enter the starting time (seconds)"
				READ(*,*) t1
				WRITE(*,*) "Please enter the ending time (seconds)"
				READ(*,*) tn
				!Display Output Table
				WRITE(*,'(/,A26,A8,A26)') line,"RESULTS",line
				WRITE(*,'(A,T9,A,T27,A,T45,A)')"Step#","Time(s)","NA(cm-3)","NB(cm-3)"
				372 FORMAT (I5,X,3(E13.6,5X)) !Defines format for output table rows
				!Set basic first result just in case n=0
				results(1) = tn
				DO i=0,n !for each step (start inclusive), calculate then write out result lines
					results(1) = t1+(i*(tn-t1)/nReal) !calculate time
					results(2) = No*(e**(0-(lambda*results(1)))) !calculate NA
					results(3) = No-results(2) !Calculate NB
					WRITE(*,372) i,results(1),results(2),results(3) !Write result line
				END DO
				WRITE(*,'(A59)') line !Draw lower border of output
			END IF
		END DO
	END IF
	
	
	
END PROGRAM APPLING_INLAB6