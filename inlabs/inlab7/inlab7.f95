PROGRAM APPLING_INLAB7

	!Author: Reece Appling
	!Date: 03/04/2021
	!Assignment Name: Inlab 7
	!Description: Takes a real number, as well as 3 integers from the user to construct 6 arrays

	IMPLICIT NONE
	
	CHARACTER(60),PARAMETER::line='============================================================'
	INTEGER::M,N,J,x,y,z
	REAL::k
	REAL,ALLOCATABLE,DIMENSION(:,:)::A,B,C,D,E,F
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders and dividers
	!INTEGER VARIABLES
		!M - Holds rows for array C/D/E
		!N - Holds columns for array C/D
		!J - Holds columns for array E
	!REAL VARIABLES
		!k - Holds real variable input from user
		!A-F - Holds arrays A-F
	!---------------End Variable Declaration--------------!
	
	!Displays program info
	WRITE(*,'(A)') "Inlab 7, developed by Reece Appling on Mar 4th 2021"

	!Prompts user to enter real # k, records, echoes
	WRITE(*,'(A)') "Please enter a real number:"
	READ(*,*) k
	WRITE(*,'(A,F12.8)') "The real number entered is k= ",k
	
	DO WHILE(.TRUE.)
		WRITE(*,'(A)') "Please enter three integers for array dimensions M, N, and J"
		READ(*,*)M,N,J
		IF((M>0).AND.(N>0).AND.(J>0)) EXIT
		WRITE(*,'(A)') "There is one or more mistakes in the input data, please try again."
	END DO
	
	!Inform no mistakes
	WRITE(*,'(A)') "Congratulations! There are no mistakes in the input data."
	
	!Allocate Arrays
	ALLOCATE(A(M,N))
	ALLOCATE(B(M,N))
	ALLOCATE(C(M,N))
	ALLOCATE(D(M,N))
	ALLOCATE(E(M,J))
	ALLOCATE(F(N,J))
	
	!Construct A,B,C,D
	DO x=1,M
		DO y=1,N
			!Construct A
			IF(x==y) THEN
				A(x,y)=1.0
			ELSE
				A(x,y)=0.5
			END IF
			!Construct B
			IF(x<=y) THEN
				B(x,y)=0.25
			ELSE
				B(x,y)=0.75
			END IF
			!Construct C
			C(x,y)=A(x,y)+B(x,y)
			!Construct D
			D(x,y)=A(x,y)*k
		END DO
	END DO
	
	!Construct F
	DO x=1,N
		DO y=1,J
			F(x,y)=1/(REAL(x)+REAL(y))
		END DO
	END DO
	
	!Construct E
	DO x=1,M
		DO y=1,J
			E(x,y)=0
			DO z=1,N
				E(x,y)=E(x,y)+(A(x,z)*F(z,y))
			END DO
		END DO
	END DO
	
	!Display results
	WRITE(*,'(A25,A7,A25)') line,"RESULTS",line
	
	!Write Array C
	WRITE(*,'(A,/)')"C="
	DO x=1,M
		DO y=1,N
			WRITE(*,'(F6.2)',ADVANCE="NO")C(x,y)
		END DO
		WRITE(*,*)
	END DO
	
	!Write Array D
	WRITE(*,'(/,A,/)')"D="
	DO x=1,M
		DO y=1,N
			WRITE(*,'(F6.2)',ADVANCE="NO")D(x,y)
		END DO
		WRITE(*,*)
	END DO
	
	!Write Array E
	WRITE(*,'(/,A,/)')"E="
	DO x=1,M
		DO y=1,J
			WRITE(*,'(F6.2)',ADVANCE="NO")E(x,y)
		END DO
		WRITE(*,*)
	END DO
	
	!Draw bottom border
	WRITE(*,'(/,A57)')line
END PROGRAM APPLING_INLAB7