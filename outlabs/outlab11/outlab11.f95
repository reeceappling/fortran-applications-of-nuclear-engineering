PROGRAM APPLING_OUTLAB11

	!Written by Reece Appling on 4/15/2021
	!Outlab 11
	!Solves for a solution vector given a tridiagonal matrix and another appropriate vector
	
	IMPLICIT NONE
	REAL,ALLOCATABLE::A(:,:),Z(:),X(:)	!A= IxI Tridiagonal Matrix, Z=RHS vector (I), X= solution vector (I)
	INTEGER::I,j !I=Matrix rank, j is an iteration variable
	
	!Set I and allocate arrays
	I=3
	ALLOCATE(A(I,I),Z(I),X(I))
	
	!Generate matrix and RHS
	CALL GENMAT(I,A,Z)
	
	!Solve for X
	CALL GAUSSELIM(I,A,Z,X)
	
	!write solution
	WRITE(*,'(A9)')"Solution:"
	DO j=1,I
		WRITE(*,'(1X,F13.9,3X)',ADVANCE="NO")X(j)
	END DO
	
END PROGRAM APPLING_OUTLAB11




SUBROUTINE GAUSSELIM(I,A,Z,X)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::I
	REAL,INTENT(IN)::A(I,I),Z(I)	!A=matrix A, Z=y values
	REAL,INTENT(OUT)::X(I)			!X=output values
	
	INTEGER::j,k	!Iteration Variables
	REAL,ALLOCATABLE::y(:),W(:)	!y=gamma values, W=z values in the worksheet
	
	!Ensure I is positive
	IF(I<=0) THEN
		WRITE(*,'(A67,I3)')"ERROR: I for gaussian elimination must be positive, entered value: ",I
		STOP "ERROR in gaussian elimination calculation"
	END IF
	
	!Ensure A is a tridiagonal matrix
	DO j=1,I
		DO k=1,I
			IF(((j-1>k).OR.(j+1<k)).AND.(A(j,k).NE.0)) THEN
				WRITE(*,'(A19,I3,A1,I3,A44,E12.6)')"Value at position (",j,",",k,") in matrix A must be zero, it is currently ",A(j,k)
				STOP "ERROR in tridiagonal matrix for gaussian elimination"
			END IF
		END DO
	END DO
	
	!Allocate vectors
	ALLOCATE(y(I-1))
	ALLOCATE(W(I))
	
	!Compute y1 and W1 if possible, else throw error
	IF(A(1,1)==0) THEN
		STOP " ERROR: A(1,1)=0, y1 and z1 are undefined"
	END IF
	y(1)=A(1,2)/A(1,1)
	W(1)=Z(1)/A(1,1)
	
	!compute y2-yI-1 (as well as Ws), if undefined throw error
	DO j=2,I-1
		IF((A(j,j)-(A(j,j-1)*y(j-1)))==0) THEN
			WRITE(*,'(A56,I2)')"ERROR: (A(j,j)-(A(j,j-1)*y(j-1))) found to be 0 when j= ",j
			STOP "ERROR in gaussian elimination calculation"
		END IF
		y(j)=A(j,j+1)/(A(j,j)-(A(j,j-1)*y(j-1)))
		W(j)=(Z(j)-(A(j,j-1)*W(j-1)))/(A(j,j)-(A(j,j-1)*y(j-1)))
	END DO
	W(I)=(Z(I)-(A(I,I-1)*W(I-1)))/(A(I,I)-(A(I,I-1)*y(I-1)))
	
	!Compute XI
	X(I)=W(I)
	
	!Compute XI-1 through X1
	DO j=I-1,1,-1
		X(j)=W(j)-(y(j)*X(j+1))
	END DO
END SUBROUTINE GAUSSELIM





SUBROUTINE GENMAT(I,A,Z)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::I
	REAL,INTENT(OUT)::A(I,I),Z(I)
	
	INTEGER::j,k
	
	!Generate diagonals, 0s, and RHS vector
	DO j=1,I
		DO k=1,I
			IF(j-1==k) THEN
				!SUBDIAGONAL
				WRITE(*,'(A11,I1,F13.9)')"Subdiagonal",j-1,A(j,k)
				A(j,k)=REAL(j)/(4.+REAL(j))
			ELSE IF(j==k) THEN
				!DIAGONAL
				A(j,k)=4./REAL(j)
				WRITE(*,'(A8,I1,F13.9)')"Diagonal",j,A(j,k)
			ELSE IF(j+1==k) THEN
				!SUPERDIAGONAL
				A(j,k)=REAL(j)/(1.+REAL(j))
				WRITE(*,'(A13,I1,F13.9)')"Superdiagonal",j,A(j,k)
			ELSE
				!NON-DIAGONALS
				A(j,k)=0
			END IF
		END DO
		!RHS vector
		Z(j)=SQRT(REAL(j))
		WRITE(*,'(A,2X,I1,2X,F13.9)')"RHS",j,Z(j)
	END DO
END SUBROUTINE GENMAT