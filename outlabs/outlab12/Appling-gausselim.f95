SUBROUTINE GAUSSELIM(I,A,Z,X)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::I
	REAL,INTENT(IN)::A(I,I),Z(I)	!A=matrix A, Z=y values
	REAL,INTENT(OUT)::X(I)			!X=output values
	
	INTEGER::j,k	!Iteration Variables
	REAL,ALLOCATABLE::gamma(:),W(:)	!gamma=gamma values, W=z values in the worksheet
	
	!Ensure I is positive
	IF(I<=0) THEN
		WRITE(*,'(A67,I3)')"ERROR: I for gaussian elimination must be positive, entered value: ",I
		STOP "ERROR in gaussian elimination calculation"
	END IF
	
	!Ensure A is a tridiagonal matrix
	DO j=1,I
		DO k=1,I
			IF(((j-1>k).OR.(j+1<k)).AND.(A(j,k).NE.0)) THEN
				WRITE(*,'(A19,I3,A1,I3,A44,E12.6)')"Value at position (",j,",",k,") in matrix A must be zero, it is currently ",A(j,k)
				STOP "ERROR in tridiagonal matrix for gaussian elimination"
			END IF
		END DO
	END DO
	
	!Allocate vectors
	ALLOCATE(gamma(I-1))
	ALLOCATE(W(I))
	
	!Compute y1 and W1 if possible, else throw error
	IF(A(1,1)==0) THEN
		STOP " ERROR: A(1,1)=0, y1 and z1 are undefined"
	END IF
	gamma(1)=A(1,2)/A(1,1)
	W(1)=Z(1)/A(1,1)
	
	!compute y2-yI-1 (as well as Ws), if undefined throw error
	DO j=2,I-1
		IF((A(j,j)-(A(j,j-1)*gamma(j-1)))==0) THEN
			WRITE(*,'(A56,I2)')"ERROR: (A(j,j)-(A(j,j-1)*gamma(j-1))) found to be 0 when j= ",j
			STOP "ERROR in gaussian elimination calculation"
		END IF
		gamma(j)=A(j,j+1)/(A(j,j)-(A(j,j-1)*gamma(j-1)))
		W(j)=(Z(j)-(A(j,j-1)*W(j-1)))/(A(j,j)-(A(j,j-1)*gamma(j-1)))
	END DO
	W(I)=(Z(I)-(A(I,I-1)*W(I-1)))/(A(I,I)-(A(I,I-1)*gamma(I-1)))
	
	!Compute XI
	X(I)=W(I)
	
	!Compute XI-1 through X1
	DO j=I-1,1,-1
		X(j)=W(j)-(gamma(j)*X(j+1))
	END DO
END SUBROUTINE GAUSSELIM