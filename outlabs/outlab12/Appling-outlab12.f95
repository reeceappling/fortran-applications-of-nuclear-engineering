PROGRAM APPLING_OUTLAB12

	!Written by Reece Appling on 4/23/2021
	!Outlab 12
	!Uses previously-made Gaussian Elimination Subroutine to solve 1D, single speed, steady state diffusion equation
	
	!The provided Gaussian elimination subroutine was not used for this program
	
	IMPLICIT NONE
	CHARACTER(80)::title
	REAL::D,Ea,rxSize,S,delta
	REAL,ALLOCATABLE::A(:,:),RHS(:),X(:)	!A= IxI Tridiagonal Matrix, Z=RHS vector (I), X= solution vector (I)
	INTEGER::I,j,k,nMesh,ioout !I=Matrix rank, j is an iteration variable
	CHARACTER(1),PARAMETER::v="|"
	CHARACTER(70),PARAMETER::tableHoriz="----------------------------------------------------------------------"
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!title - Holds name of the case to be executed
	!CHARACTER PARAMETERS
		!tableHoriz - Horizontal borders and dividers for output tables
		!v - Vertical dividers for output tables
	!INTEGER VARIABLES
		! I - Matrix rank
		! j/k - iteration variables
		! nMesh - number of mesh cells
		! ioout - output file IO state
	!REAL VARIABLES
		!D - diffusion coefficient
		!Ea - Macroscopic Absorption cross-section
		!rxSize - length of reactor
		!S - source strength
		!delta - step size for cells
		!A - Matrix for use with gaussian elimination
		!RHS - vector for use in gaussian elimination
		!X - Solution vactor from gaussian elimination
	!---------------End Variable Declaration--------------!
	
	!Get all information (ideally from piped-in file)
	READ(*,'(A80)')title
	READ(*,'(F4.1,A)')D
	READ(*,'(F4.1,A)')Ea
	READ(*,'(F4.1,A)')rxSize
	READ(*,'(F4.1,A)')S
	READ(*,'(I3,A)')nMesh
	
	!write title
	WRITE(*,'(A80)')title
	
	!write inputs table
	WRITE(*,'(A70)')tableHoriz
	372 FORMAT (A1,T3,A,T50,A,2X,F13.9,T70,A)
	WRITE(*,372)v,"Diffusion coefficient (cm)",v,D,v
	WRITE(*,372)v,"Macroscopic Absorption cross section (/cm)",v,Ea,v
	WRITE(*,372)v,"Reactor Size (cm)",v,rxSize,v
	WRITE(*,372)v,"Neutron Source Strength (/cm)",v,S,v
	WRITE(*,'(A1,T3,A,T50,A,2X,I3,T70,A)')v,"Number of mesh cells",v,nMesh,v
	WRITE(*,'(A70)')tableHoriz
	
	
	!Ensure all input data works, error checking
	IF((D<=0.0).OR.(nMesh<=0).OR.(Ea<=0.0).OR.(rxSize<=0.0).OR.(S<0.0)) THEN
		IF(D<=0.0) WRITE(*,'(A53)')"Error: Diffusion coefficient must be greater than 0.0"
		IF(nMesh<=0) WRITE(*,'(A50)')"Error: Number of mesh cells must be greater than 0"
		IF(Ea<=0.0) WRITE(*,'(A57)')"Error: Macroscopic cross section must be greater than 0.0"
		IF(rxSize<=0.0) WRITE(*,'(A44)')"Error: Reactor size must be greater than 0.0"
		IF(S<0.0) WRITE(*,'(A86)')"Source strength must be greater than or equal to 0.0"
		STOP "ERROR: invalid value for one or more of the following, D,I,Ea,a,S"
	ELSE
		WRITE(*,*)"Congratulations on entering proper input values. Code execution will now proceed"
	END IF
	
	!Calculate Delta
	delta = rxSize/REAL(nMesh)
	
	!Set I and allocate arrays
	I = nMesh-1
	ALLOCATE(A(I,I),RHS(I),X(I))
	
	!Generate A and RHS
	!Generate sub/super/diagonals, 0s, and RHS vector
	DO j=1,I
		DO k=1,I
			IF((j-1==k).OR.(j+1==k)) THEN
				A(j,k)=0-(1/(delta**2))!SUBDIAGONAL OR SUPERDIAGONAL
			ELSE IF(j==k) THEN
				A(j,k)=(2/(delta**2))+(Ea/D)!DIAGONAL
			ELSE
				A(j,k)=0!NON-DIAGONALS
			END IF
		END DO
		!RHS vector
		RHS(j)=S/D
	END DO
	WRITE(*,*)
	
	!Solve using Gaussian elimination
	CALL GAUSSELIM(I,A,RHS,X)
	
	!write solutions
	WRITE(*,'(A9)')"Solution:"
	WRITE(*,'(T10,I2,2X,F13.9)')0,0.0
	DO j=1,I
		WRITE(*,'(T10,I2,2X,F13.9)')j,X(j)
	END DO
	WRITE(*,'(T10,I2,2X,F13.9)')I,0.0
	
	!Sneakily write solutions for real problem to other file
	OPEN(UNIT=12,FILE="gra12.txt",STATUS='REPLACE',ACTION='WRITE',IOSTAT=ioout)
	!Check for errors opening output file
	IF (ioout .NE. 0) THEN
					WRITE(*,*)"Error opening output file, looping back to output type selection"
	ELSE
	END IF
	
END PROGRAM APPLING_OUTLAB12