

NOTE: moodle would not accept more than 4 files, so the files are zipped into one

The provided Gaussian elimination subroutine was not used for this program

In order to compile this code:
Make sure the following files are in the directory it will be run in:
	Appling-gausselim.f95
	Appling-outlab12.f95
	makefile.mak
	inp12.txt
make -f makefile.mak
This will first compile the gaussian elimination object into gauss.o, then compile the main program along with gauss.o into Appling-outlab12.out 

To run the program,
	First make sure inp12.txt is in the same directory as Appling-outlab12
	To output to screen:
		./Appling-outlab12.out  <inp12.txt
	To output to FILENAME:
		./Appling-outlab12.out  <inp12.txt >FILENAME