PROGRAM APPLING_OUTLAB2

	!Author: Reece Appling
	!Date: 1/29/2021
	!Assignment Name: OutLab 2
	!Assignment Description: Calculates electron energies at five scattering angles for collisions with a heavy charged particle of known mass and kinetic energy.
	
	IMPLICIT NONE
	
	CHARACTER(30)::hcpName
	CHARACTER(1),PARAMETER::v="|"
	CHARACTER(66),PARAMETER::tableHoriz="------------------------------------------------------------------"
	REAL::hcpMass,hcpKE
	REAL,PARAMETER::me=5.486E-4,PI=3.14159
	
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!hcpName - Holds name of Heavy Charged Particle
	!CHARACTER PARAMETERS
		!v - Vertical borders and dividers for output table
		!tableHoriz - Horizontal borders and dividers for output table
	!REAL VARIABLES
		!hcpMass - Holds mass (amu) of heavy charged particle
		!hcpKE - Holds kinetic energy (MeV) of heavy charged particle
	!REAL PARAMETERS
		!me - Mass of an Electron (amu)
		!PI - Mathematical constant PI
	!---------------End Variable Declaration--------------!


	!Prompt user to enter type of heavy charged particle
	WRITE(*,*) "Enter type of heavy charged particle (e.g. alpha): "
	READ(*,'(A30)') hcpName !Read name of heavy charged particle from user


	!Prompt user to input mass of heavy charged particle (amu)
	WRITE(*,*) "Enter mass of heavy charged particle (amu): "
	READ(*,*) hcpMass !Read mass of heavy charged particle from user
	
	
	!Prompt user to input kinetic energy of heavy charged particle (MeV)
	WRITE(*,*) "Enter kinetic energy of heavy charged particle (MeV): "
	READ(*,*) hcpKE !Read kinetic energy of heavy charged particle from user
	
	
	!Write Start of output, header and user's inputs
	WRITE(*,"(2/,A)") "Scattering of heavy charge particle by an electron: programmed by Reece Appling" !Write 2 blank lines then header
	WRITE(*,"(5X,A,T36,A,A)")"Charged Particle Type","=   ",hcpName !Write charged particle type
	WRITE(*,"(5X,A,T36,A,F8.4)")"Charged Particle Mass (amu)","= ",hcpMass !Write charged particle mass
	WRITE(*,"(5X,A,T36,A,F8.4)")"Charged Particle Energy (MeV)","= ",hcpKE !Write charged particle kinetic energy
	
	
	!Draw output table head
	WRITE(*,"(/,A66)")tableHoriz !Draw top horizontal border
	WRITE(*,"(A,2X,A,T41,A,2X,A,T66,A)")v,"Electron Scattering angle (Radians)",v,"Electron Energy (MeV)",v
	WRITE(*,"(A66)")tableHoriz !Draw horizontal line below headers
	
	
	!Draw table body (data rows in table for each of the 5 angles, then the bottom border)
	372 FORMAT (A,4X,A,T41,A,3X,E11.4,T66,A) !Defines format for output table rows
	WRITE(*,372)v,"0",v,((4*me/hcpMass)*hcpKE*(COS(0.0)**2)),v
	WRITE(*,372)v,"pi/4",v,(4*me/hcpMass)*hcpKE*(COS(PI/4)**2),v
	WRITE(*,372)v,"pi/2",v,(4*me/hcpMass)*hcpKE*(COS(PI/2)**2),v
	WRITE(*,372)v,"3 pi/4",v,(4*me/hcpMass)*hcpKE*(COS(3*PI/4)**2),v
	WRITE(*,372)v,"pi",v,(4*me/hcpMass)*hcpKE*(COS(PI)**2),v
	WRITE(*,"(A66)")tableHoriz !Draw bottom horizontal border
END PROGRAM APPLING_OUTLAB2