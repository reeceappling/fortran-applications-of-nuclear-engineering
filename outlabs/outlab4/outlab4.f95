PROGRAM APPLING_OUTLAB4

	!Author: Reece Appling
	!Date: 2/17/2021
	!Assignment Name: Outlab 4
	!Assignment Description: Calculates Emergent energy(s) of an inelastic neutron scattering event given:
	!	initial neutron kinetic energy, nucleus mass number, and scattering angle cosine
	
	IMPLICIT NONE
	
	CHARACTER(72)::xThermic
	INTEGER::A,errors=0
	REAL::Q,nKE,angleCos,AReal,termOne,termTwo,posSoln,negSoln
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!xThermic - Holds string, normally acceptable values "endothermic" and "exothermic"
	!INTEGER VARIABLES
		!A - Holds mass number of the target nucleus
		!errors - Holds number of detected errors, starts as 0
	!REAL VARIABLES
		!Q - Holds the Q-value (MeV) of the reaction
		!nKE - Holds the kinetic energy of the incedent neutron (MeV)
		!angleCos - Holds the scattering angle cosine
		!AReal - Holds the REAL value of the INTEGER A
		!termOne - Holds the first term on the right side of the SQRT(emergent energy) equation
		!termTwo - Holds the term under the square root on the right side of the SQRT(emergent energy) equation
		!posSoln - termOne+SQRT(termTwo)=SQRT(Eo) for the positive numerical solution
		!negSoln - termOne-SQRT(termTwo)=SQRT(Eo) for the negative numerical solution
	!---------------End Variable Declaration--------------!
	
	
	
	
	!Prompts user to enter mass number of taget nucleus, then records it
	WRITE(*,'(2/,A)') "Enter mass number of target nucleus:"
	READ(*,*) A
	AReal = REAL(A) !Sets real type version of A

	!Prompt user to enter the name of the heavy charged particle
	WRITE(*,*) "Is this reaction endothermic or exothermic?"
	READ(*,'(A72)') xThermic !Read name of heavy charged particle from user

	!Prompts user to enter the Q value (MeV) of the reaction, then records it
	WRITE(*,*) "Enter reaction Q value (MeV):"
	READ(*,*) Q
	
	!Prompts user to enter incedent neutron KE (MeV), then records it
	WRITE(*,*) "Enter kinetic energy of incedent neutron (MeV):"
	READ(*,*) nKE
	
	!Prompts user to enter the cosine of the scattering angle [-1,1], then records it
	WRITE(*,*) "Enter scattering angle cosine:"
	READ(*,*) angleCos
	
	!Writes program title and author, then summarizes user inputs
	WRITE(*,'(2/,A)') "Neutron Inelastic Scattering: programmed by Reece Appling"
	WRITE(*,'(3X,A,T44,A,I3)') "Mass number of target nucleus","= ",A
	WRITE(*,'(3X,A,T44,A,A)') "Reaction Type","= ",xThermic
	WRITE(*,'(3X,A,T44,A,E13.6)') "Reaction Q value (MeV)","= ",Q
	WRITE(*,'(3X,A,T44,A,E13.6)') "Incedent neutron kinetic energy (MeV)","= ",nKE
	WRITE(*,'(3X,A,T44,A,F8.5)') "Scattering angle cosine in lab system","= ",angleCos
	
	!Checks _Thermic input against Q to appropriately display necessary warnings/errors
	SELECT CASE(xThermic)
		CASE("exothermic")
			IF(Q<0)WRITE(*,*)"WARNING: Q-value < 0 implies endothermic"
		CASE("endothermic")
			IF(Q>0)WRITE(*,*)"WARNING: Q-value > 0 implies exothermic"
		CASE DEFAULT
			WRITE(*,*)"ERROR: Reaction input was neither endothermic nor exothermic"
			errors=errors+1
	END SELECT
	
	!If Q=0 display warning that the event is elastic
	IF(Q==0) THEN
		WRITE(*,*)"WARNING: Q=0, the event is therefore elastic"
		errors=errors+1
	END IF
	
	!Ensure A is positive
	IF(A<=0) THEN
		WRITE(*,*)"ERROR: Mass number must be a positive integer"
		errors=errors+1
	END IF
	
	!Ensure angleCos is within [-1,1]
	IF((angleCos<-1).OR.(angleCos>1)) THEN
		WRITE(*,*)"ERROR: Scattering angle cosine must be in interval [-1,1]"
		errors=errors+1
	END IF
	
	!If there are errors, terminate gracefully, otherwise continue
	IF(errors>0) THEN
		WRITE(*,'(A,I1,A)')"Execution will be terminated due to ",errors," improper input values"
		STOP "Fatal Error Encountered"
	ELSE !No errors, continuing execution, notifying then calculating RHS terms
		WRITE(*,*)"Congratulations on entering proper input values. Code execution will now proceed"
		termOne=(angleCos/(AReal+1))*SQRT(nKE)
		termTwo=((angleCos/(AReal+1))**2)*nKE
		termTwo=termTwo+((AReal-1)*nKE/(AReal+1))
		termTwo=termTwo+((AReal*Q)/(AReal+1))
		!If reaction is not permissible, notify and terminate
		IF(termTwo<0) THEN
			WRITE(*,*)"Nuclear reaction is not permissible with the input values"
			STOP "Successful termination of code execution"
		ELSE !Reaction is permissible, calculate solution variables (sqrt of answer(s)) and continue
			posSoln=termOne+SQRT(termTwo)
			negSoln=termOne-SQRT(termTwo)
			IF(negSoln<0) THEN !If the negative solution is less than 0, only the positive solution is valid, notify
				WRITE(*,'(A,E13.6)')"Only one solution premitted: scattered neutron energy (MeV) = ",posSoln**2
			ELSE !Otherwise Both solutions are valid, notify
				WRITE(*,'(A,2E13.6)')"Two solutions premitted: scattered neutron energy (MeV) = ",posSoln**2,negSoln**2
			END IF
		END IF
	END IF
END PROGRAM APPLING_OUTLAB4