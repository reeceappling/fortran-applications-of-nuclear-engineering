PROGRAM APPLING_OUTLAB6

	!Author: Reece Appling
	!Date: 2/26/2021
	!Assignment Name: Outlab 6
	!Assignment Description: Approximate SIN(x) by using Taylor series

	IMPLICIT NONE
	
	CHARACTER(70),PARAMETER::line='======================================================================'
	INTEGER::errors=0,n,nTerms,factorial,i
	REAL::x,sc,trunc=0.0,eqn
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders and dividers
	!INTEGER VARIABLES
		!errors - Holds number of detected input errors, starts as 0
		!n - Holds iteration index for the summing of Taylor series
		!nTerms - Holds max number of terms in Taylor series
		!i - Holds iteration index for calculating factorials
		!factorial - Holds current value of a calculat(ed/ing) factorial
	!REAL VARIABLES
		!x - Holds angle in radians to estimate the sine of
		!sc - Holds stopping criterion
		!trunc - Holds value of current truncated form of equation
		!eqn - a single summation term in the series, without multiplying by (-1)**n
	!---------------End Variable Declaration--------------!
	
	!Displays Header Information
	WRITE(*,'(A60)') line
	WRITE(*,'(A)') "This code was developed by Reece Appling on Feb 26th 2021"
	WRITE(*,'(A)') "The purpose of this code is to approximate SIN(x) by using Taylor series"
	WRITE(*,'(A60)') line
	
	!Prompts user to enter angle (radians) whose SIN to evaluate, then records it
	WRITE(*,'(/,A)') "Please enter angle (radians) whose SIN you wish to evaluate:"
	READ(*,*) x
	
	!Prompts user to enter stopping criterion, then records it
	WRITE(*,'(A)') "Please enter the stopping criterion:"
	READ(*,*) sc
	
	!Prompts user to enter maximum number of terms in the Taylor series, then records it
	WRITE(*,'(A)') "Please enter the maximum number of terms in the Taylor series:"
	READ(*,*) nTerms
	
	!Display input data
	WRITE(*,'(/,A25,A10,A25)') line,"INPUT DATA",line
	WRITE(*,'(A,F10.6)') "Angle (radians) =",x
	WRITE(*,'(A,E13.6)') "Stopping Criterion = ",sc
	WRITE(*,'(A,I2)') "Maximum number of terms = ",nTerms
	WRITE(*,'(A60,/)') line !lower border of input data followed by space before error checks
	
	!Check input data
	!If radians are outside of (-1,1), warn+error
	IF(x**2>=1) THEN
		errors=errors+1
		WRITE(*,*) "Error: Angle (radians) must be between -1 and 1 (non-inclusive)"
	END IF
	
	!If stopping criterion <=0, warn+error
	IF(sc<=0) THEN
		errors=errors+1
		WRITE(*,*) "Error: Stopping criterion must be greater than 0"
	END IF
	
	!If max # terms <1, warn+error
	IF(nTerms<1) THEN
		errors=errors+1
		WRITE(*,*) "Error: Maximum number of terms in the Taylor series must be greater than 0"
	END IF
	
	!If errors exist, exit program, otherwise continue
	IF(errors>0) THEN !Display number of errors and exit program
		WRITE(*,'(A,I1)') "Fatal errors detected in user inputs: ",errors
		STOP ': Program terminated due to input errors'
	END IF
	
	!Notify inputs valid
	WRITE(*,*) "Congratulations! There were no erroneous inputs."
	
	!Loop to calculate trancated equation sections, sum them, and check against stopping criterion
	DO n=0,nTerms-1 !MUST be nTerms-1 because index n begins at 0
		!Calculate factorial of 2n+1
		factorial = 1 !first set factorial to 1 before new factorial calc
		DO i=1,((2*n)+1)
			factorial = factorial*i
		END DO
		!Calculate RHS of sum term for n, as well as the new truncated estimate at n
		eqn = (REAL(x)**((2*REAL(n))+1))/REAL(factorial)
		trunc = trunc+(((-1)**REAL(n))*eqn)
		IF(eqn<=sc) EXIT
	END DO
	
	!Notify if series converged or not, if it did then set the nTerms now to the convergence number
	IF(eqn <=sc) WRITE(*,"(A,I2,A)") "Taylor series converged successfully in ",n+1," terms"
	IF(eqn > sc) WRITE(*,"(A,I2,A)") "Taylor series did not converge successfully in ",nTerms," terms"
	
	!Draw Results
	WRITE(*,'(/,A31,A7,A32)') line,"RESULTS",line !Header/top border
	WRITE(*,'(A,T55,F13.9)') "The value of SIN(x) with the truncated Taylor series: ",trunc
	WRITE(*,'(A,T55,F13.9)') "The intrinsic-function value of SIN(x) is ",SIN(x)
	WRITE(*,'(A,T56,E15.8)') "The difference between these two values is ",SIN(x)-trunc
	WRITE(*,'(A70,/)') line !Draw bottom border of output then blank line
END PROGRAM APPLING_OUTLAB6