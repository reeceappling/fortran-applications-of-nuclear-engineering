PROGRAM APPLING_OUTLAB8

	!Author: Reece Appling
	!Date: 4/05/2021
	!Assignment Name: Outlab 8
	!Assignment Description: When a user inputs 3 positive integers,
		!arrays and vectors can be constructed and the user can select operations to be done with them

	IMPLICIT NONE
	
	CHARACTER(70),PARAMETER::line='======================================================================'
	CHARACTER(10)::operation
	INTEGER::M,N,J,i,k
	REAL::d
	REAL,ALLOCATABLE::A(:),B(:),C(:),X(:,:),Y(:,:),Z(:,:)
	REAL,EXTERNAL::DotProd
	!------------------Variable Declaration---------------!
	!CHARACTER PARAMETERS
		!line - Horizontal borders and dividers
	!CHARACTER VARIABLES
		!operation - Used to select which operations to be done on arrays/vectors
	!INTEGER VARIABLES
		!M,N,J - Array dimensions
		!i,k - iteration variables
	!REAL VARIABLES
		!d - dotprod solution
		!A,B,C,X,Y,Z - Arrays for use throughout the program
	!REAL FUNCTIONS
		!DotProd - takes dot product of two Arrays
	!---------------End Variable Declaration--------------!
	
	!Displays Header Information
	WRITE(*,'(A)') "This code was developed by Reece Appling on Apr 5th 2021"
	
	!Get inputs from user and verify validity
	CALL PROCESSINPUTS(M,N,J,operation,line)
	
	!Allocate vector and matrix arrays
	ALLOCATE(A(M),B(M),C(N),X(N,M),Y(M,J),Z(N,J))
	
	!Initialize vector and matric array values
	CALL INITIALIZE(M,N,J,A,B,X,Y)
	
	!Do operations based on case selected
	SELECT CASE(operation)
		!Do MatMatProd and output Z matrix
		CASE("MatMatProd")
			CALL MatMatProd(N,M,J,X,Y,Z)
			WRITE(*,'(A27,A7,A26)') line,"RESULTS",line
			WRITE(*,'(2(A,/),A)')"The operation is matrix-matrix-product","The resulting matrix is:","Z="
			DO i=1,N
				DO k=1,J
					WRITE(*,'(X,F6.2)',ADVANCE='NO')Z(i,k)
				END DO
				WRITE(*,'(/)',ADVANCE='NO')
			END DO
		!Do MatVecProd and output C Vector
		CASE("MatVecProd")
			CALL MatVecProd(N,M,X,A,C)
			WRITE(*,'(A27,A7,A26)') line,"RESULTS",line
			WRITE(*,'(2(A,/),A)')"The operation is matrix-vector-product","The resulting array is:","C="
			DO i=1,N
				WRITE(*,'(X,F6.2)',ADVANCE='NO')C(i)
			END DO
			WRITE(*,*)
		!Do DotProd and output d scalar
		CASE("DotProd")
			d=DotProd(A,B,M)
			WRITE(*,'(A27,A7,A26)') line,"RESULTS",line
			WRITE(*,'(2(A,/),A,F6.2)')"The operation is dot-product","The resulting value is:","d= ",d
		!Default case should never happen but will terminate program
		CASE DEFAULT
			WRITE(*,'(3A)')"Error: chosen operation ",operation," is not one of the three choices. Press any key to terminate"
			READ(*,*)
			STOP
	END SELECT
	WRITE(*,'(2/)')!Write 2 blank lines
END PROGRAM APPLING_OUTLAB8


!gets user inputs and validates them
SUBROUTINE PROCESSINPUTS(M,N,J,operation,line)
	IMPLICIT NONE
	INTEGER,INTENT(OUT)::M,N,J
	CHARACTER(10),INTENT(OUT)::operation
	CHARACTER(70),INTENT(IN)::line
	
	!Prompts user to enter M, N, and J
	WRITE(*,'(/,A)') "Please enter 3 integers for array dimensions M, N, and J:"
	READ(*,*) M,N,J
	
	!Prompts user to enter matrix operation, reads it
	WRITE(*,'(A)') "Please enter a desired operation among the following three choices (Case Sensitive):"
	WRITE(*,'(2X,A)') "Matrix-matrix-product: MatMatProd"
	WRITE(*,'(2X,A)') "Matrix-vector-product: MatVecProd"
	WRITE(*,'(2X,A)') "Vector-vector dot-product: DotProd"
	READ(*,*)operation
	
	!Echo inputs
	WRITE(*,'(A27,A6,A27)') line,"INPUTS",line
	WRITE(*,'(3(A,I3))')"Three entered integers: M= ",M,", N= ",N,", and J= ",J
	WRITE(*,'(2A)')"The entered operation: ",operation
	
	!Ensure M,N,J are positive
	IF((M<=0).OR.((N<=0).OR.(J<=0))) THEN
		WRITE(*,'(A)')"Error: M, N, and J must all be positive integers. Press any key to terminate program"
		READ(*,*)
		STOP
	END IF
	
	!Ensure operation is valid
	IF((.FALSE.).EQV.((operation.EQ."MatMatProd").OR.(operation.EQ."MatVecProd").OR.(operation.EQ."DotProd"))) THEN
		WRITE(*,'(3A)')"Error: chosen operation ",operation," is not one of the three choices. Press any key to terminate"
		READ(*,*)
		STOP
	END IF
	
	!Notify if all inputs are valid
	WRITE(*,'(A,/)')"Congratulations! There are no erroneous inputs"
END SUBROUTINE PROCESSINPUTS


!Set initial values for all arrays
SUBROUTINE INITIALIZE(M,N,J,A,B,X,Y)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::M,N,J
	REAL,INTENT(OUT)::A(M),B(M),X(N,M),Y(M,J)
	INTEGER::i,k
	
	!Set A and B values (A(i)=i,B(i)=1/i)
	DO i=1,M
		A(i)=REAL(i)
		B(i)=1/REAL(i)
	END DO
	
	!Set Matrix X values
	DO i=1,N
		DO k=1,M
			IF(i.EQ.k) THEN
				X(i,k)=1.0
			ELSE IF(i>k) THEN
				X(i,k)=1.0/REAL(i)
			ELSE
				X(i,k)=1.0/REAL(k)
			END IF
		END DO
	END DO
	
	!Set Matrix Y values
	DO i=1,M
		DO k=1,J
			IF(i.EQ.k) THEN
				Y(i,k)=1.0/REAL(i)
			ELSE IF(i>k) THEN
				Y(i,k)=1.0
			ELSE
				Y(i,k)=-1.0
			END IF
		END DO
	END DO
END SUBROUTINE INITIALIZE


!Subroutine for MatMatProd
SUBROUTINE MatMatProd(N,M,J,X,Y,Z)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::M,N,J
	REAL,INTENT(IN)::X(N,M),Y(M,J)
	REAL,INTENT(OUT)::Z(N,J)
	INTEGER::i,k,l
	REAL::tempSum
	
	DO i=1,N
		DO k=1,J
			tempSum=0
			DO l=1,M
				tempSum=tempSum+(X(i,l)*Y(l,k))
			END DO
			Z(i,k)=tempSum
		END DO
	END DO
END SUBROUTINE MatMatProd


!Subroutine for MatVecProd
SUBROUTINE MatVecProd(N,M,X,A,C)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::M,N
	REAL,INTENT(IN)::X(N,M),A(M)
	REAL,INTENT(OUT)::C(N)
	INTEGER::i,k,l
	REAL::tempSum
	
	DO i=1,N
		tempSum=0
		DO l=1,M
			tempSum=tempSum+(X(i,l)*A(l))
		END DO
		C(i)=tempSum
	END DO
END SUBROUTINE MatVecProd


!DotProd function
REAL FUNCTION DotProd(A,B,M)
	IMPLICIT NONE
	INTEGER::M
	REAL::A(M),B(M)
	INTEGER::i
	
	DotProd=0
	DO i=1,M
		DotProd=DotProd+(A(i)*B(i))
	END DO
END FUNCTION