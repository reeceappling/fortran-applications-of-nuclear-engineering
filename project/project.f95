PROGRAM APPLING_CHECKPOINT4

	!Author: Reece Appling
	!Date: 04/28/2021
	!Assignment Name: Checkpoint 4
	!Assignment Description: Allows user to input via console or file: parent isotope initial density, scalar flux of neutrons, 
	!	absorption cross sections for both parent and daughter, and parent capture cross section. Then the user can test variations of times and timesteps 
	!	where the densities of parent and daughter at each timestep can be output to either the screen or a file

	IMPLICIT NONE
	
	CHARACTER(72)::caseTitle,fileOut
	CHARACTER(86),PARAMETER::line="--------------------------------------------------------------------------------------"
	INTEGER::inType,outType
	REAL::flux,parentDensity,crsAp,crsCp,crsAd,lambda
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!caseTitle - Holds name of the case to be executed
		!fileOut - Holds name of output file, ex: "output.txt"
	!CHARACTER PARAMETERS
		!line - Horizontal borders and dividers for output table
	!INTEGER VARIABLES
		!inType -  Holds selector 0/1 for input data from screen/file
		!outType - Holds selector 0/1 for output data to screen/file
	!REAL VARIABLES
		!flux - Holds the thermal neutron flux (#/s cm^2)
		!parentDensity - Holds the initial atomic density of parent species (#/cm^3)
		!crsAp - Holds the microscopic absorption cross section of parent species (b)
		!crsCp - Holds the microscopic capture cross section of parent species (b)
		!crsAd - Holds the microscopic absorption cross section of daughter species (b)
		!lambda - Holds the decay constant of daughter species (/s)
	!---------------End Variable Declaration--------------!

	!Print header
	CALL HEADER()
	
	!Read inputs and echoes them
	CALL ReadEchoInput(inType,outType,caseTitle,flux,parentDensity,crsAp,crsCp,crsAd,lambda,line,fileOut)

	!Verify validity of all inputs
	CALL VERIFYINPUT(flux,parentDensity,crsAp,crsCp,crsAd,lambda,outType)
	
	!Calculate all necessary values and output them as necessary
	CALL CalculateAndOutput(inType,outType,line,parentDensity,flux,crsAp,crsAd,crsCp,lambda)
	
	!Call the end program subroutine to output final commentary
	CALL ProgramEnd(outType,fileOut)
END PROGRAM APPLING_CHECKPOINT4


!Outputs header to screen
SUBROUTINE HEADER()
	IMPLICIT NONE
	WRITE(*,*)"Checkpoint 4, programmed by Reece Appling on April 28th, 2021"
	WRITE(*,*)"Revised from checkpoint 3, programmed by Reece Appling on April 7th, 2021"
END SUBROUTINE HEADER


!Reads inputs from user then user or file as necessary and echoes all values required
SUBROUTINE ReadEchoInput(inType,outType,caseTitle,flux,parentDensity,crsAp,crsCp,crsAd,lambda,line,fileOut)
	IMPLICIT NONE
	
	INTEGER,INTENT(OUT)::inType,outType
	CHARACTER(72),INTENT(OUT)::caseTitle,fileOut
	REAL,INTENT(OUT)::flux,parentDensity,crsAp,crsCp,crsAd,lambda
	
	CHARACTER(1)::v="|"
	CHARACTER(86),INTENT(IN)::line
	
	CHARACTER(70)::fileIn
	INTEGER::ioout,ioin
	
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!caseTitle - Holds name of the case to be executed
		!fileOut - Holds name of output file, ex: "output.txt"
		!fileIn - Holds name of input file, ex: "input.txt"
	!CHARACTER PARAMETERS
		!v - Vertical borders and dividers for output table
		!line - Horizontal borders and dividers for output table
	!INTEGER VARIABLES
		!inType -  Holds selector 0/1 for input data from screen/file
		!outType - Holds selector 0/1 for output data to screen/file
		!ioout - IOSTAT for output file
		!ioin - IOSTAT for input file
	!REAL VARIABLES
		!flux - Holds the thermal neutron flux (#/s cm^2)
		!parentDensity - Holds the initial atomic density of parent species (#/cm^3)
		!crsAp - Holds the microscopic absorption cross section of parent species (b)
		!crsCp - Holds the microscopic capture cross section of parent species (b)
		!crsAd - Holds the microscopic absorption cross section of daughter species (b)
		!lambda - Holds the decay constant of daughter species (/s)
	!---------------End Variable Declaration--------------!
	
	!Set initial in/outType values to invalid temporary numbers, fileOut to a placeholder string
	inType=-1
	outType=-1
	fileOut="checkpoint4out.txt"!TEMPORARY VALUE!!!!
	
	!Prompt user on how to input data and record, if input type is incorrect notify and retry
	DO
		WRITE(*,*) "Enter 0/1 to enter input data from screen/file:"
		READ(*,*) inType
		IF((inType.EQ.0).OR.(inType.EQ.1)) THEN
			!If input type is file, ask for file name, then open file
			IF(inType.EQ.1) THEN
				WRITE(*,*) "Please enter the input file name"
				READ(*,*) fileIn
				OPEN(UNIT=11,FILE=fileIn,STATUS='OLD',ACTION='READ',IOSTAT=ioin)
				!Check for errors opening input file
				IF (ioin .NE. 0) THEN
					WRITE(*,*)"Error opening input file, looping back to input type selection"
				ELSE
					EXIT
				END IF
			ELSE
				EXIT
			END IF
		ELSE
			WRITE(*,'(/,A)')"Error: input type must be either 0 (screen), or 1 (file). Please try again"
		END IF
	END DO
	
	!Prompt user on how to output data and record, if output type is incorrect notify and retry
	DO
		WRITE(*,*) "Enter 0/1 to enter output data to screen/file:"
		READ(*,*) outType
		IF((outType.EQ.0).OR.(outType.EQ.1)) THEN
			!If output type is file, ask for file name, then open file
			IF(outType.EQ.1) THEN
				WRITE(*,*) "Please enter the output file name"
				READ(*,*) fileOut
				OPEN(UNIT=12,FILE=fileOut,STATUS='REPLACE',ACTION='WRITE',IOSTAT=ioout)
				!Check for errors opening input file
				IF (ioout .NE. 0) THEN
					WRITE(*,*)"Error opening output file, looping back to output type selection"
				ELSE
					EXIT
				END IF
			ELSE
				EXIT
			END IF
		ELSE
			WRITE(*,'(/,A)')"Error: output type must be either 0 (screen), or 1 (file). Please try again"
		END IF
	END DO
	
	!Defines format for table rows of input data
	372 FORMAT (A,2X,A,T65,A,3X,E13.6,T86,A) 
	!Read input data based on user selection
	SELECT CASE(inType)
		!For case of input from screen...
		CASE(0)
			!Prompt user to enter the title of the case to execute
			WRITE(*,*) "Enter title of executed case (Max 72 Characters)"
			READ(*,'(A72)') caseTitle !Read title of the case to execute from user
		
			!Prompt user to input the thermal neutron flux (#/s cm^2)
			WRITE(*,*) "Enter thermal neutron flux (#/s cm^2): "
			READ(*,*) flux !Read thermal neutron flux from user
	
			!Prompt user to input the initial atomic density of parent species (#/cm^3)
			WRITE(*,*) "Enter initial atomic density of parent species (#/cm^3): "
			READ(*,*) parentDensity !Read initial atomic density of parent species from user
	
			!Prompt user to input the microscopic absorption cross section of parent species (b)
			WRITE(*,*) "Enter microscopic absorption cross section of parent species (b): "
			READ(*,*) crsAp !Read microscopic absorption cross section of parent species from user
	
			!Prompt user to input the microscopic capture cross section of parent species (b)
			WRITE(*,*) "Enter microscopic capture cross section of parent species (b): "
			READ(*,*) crsCp !Read microscopic capture cross section of parent species from user
	
			!Prompt user to input the microscopic absorption cross section of daughter species (b)
			WRITE(*,*) "Enter microscopic absorption cross section of daughter species (b): "
			READ(*,*) crsAd !Read microscopic absorption cross section of daughter species from user
	
			!Prompt user to input the decay constant of daughter species (/s)
			WRITE(*,*) "Enter decay constant of daughter species (/s): "
			READ(*,*) lambda !Read decay constant of daughter species from user
			
			!Output table of input values
			WRITE(*,'(A72)')caseTitle
			WRITE(*,"(A86)")line !Draw top horizontal border
			WRITE(*,372)v,"Thermal neutron flux (#/s cm^2)",v,flux,v
			WRITE(*,372)v,"Atomic density of parent species (#/cm^3)",v,parentDensity,v
			WRITE(*,372)v,"Microscopic absorption cross section of parent species (b)",v,crsAp,v
			WRITE(*,372)v,"Microscopic capture cross section of parent species (b)",v,crsCp,v
			WRITE(*,372)v,"Microscopic absorption cross section of daughter species (b)",v,crsAd,v
			WRITE(*,372)v,"Decay constant of daughter species (/s)",v,lambda,v
			WRITE(*,"(A86)")line !Draw bottom horizontal border
			
		!For case of input from file, read in all basic input values
		CASE(1)
			!Read in all basic input info (not run specific)
			READ(11,'(A72)')caseTitle
			READ(11,*)flux
			READ(11,*)parentDensity
			READ(11,*)crsAp
			READ(11,*)crsCp
			READ(11,*)crsAd
			READ(11,*)lambda
			
			!Output table of input values
			WRITE(12,'(A72)')caseTitle
			WRITE(12,"(A86)")line !Draw top horizontal border
			WRITE(12,372)v,"Thermal neutron flux (#/s cm^2)",v,flux,v
			WRITE(12,372)v,"Atomic density of parent species (#/cm^3)",v,parentDensity,v
			WRITE(12,372)v,"Microscopic absorption cross section of parent species (b)",v,crsAp,v
			WRITE(12,372)v,"Microscopic capture cross section of parent species (b)",v,crsCp,v
			WRITE(12,372)v,"Microscopic absorption cross section of daughter species (b)",v,crsAd,v
			WRITE(12,372)v,"Decay constant of daughter species (/s)",v,lambda,v
			WRITE(12,"(A86)")line !Draw bottom horizontal border
			
		!Default Case, SHOULD NEVER OCCUR DUE TO PREVIOUS CHECKS
		CASE DEFAULT
			WRITE(*,*)"Error: input type found to be neither 0 nor 1, this error should never occur. Terminating program."
			STOP
	END SELECT
END SUBROUTINE ReadEchoInput

!Verifies all basic user or file inputs, stopping program if errors present
SUBROUTINE VERIFYINPUT(flux,parentDensity,crsAp,crsCp,crsAd,lambda,outType)
	IMPLICIT NONE
	
	REAL,INTENT(IN)::flux,parentDensity,crsAp,crsCp,crsAd,lambda
	INTEGER,INTENT(IN)::outType
	
	INTEGER::errors=0
	
	!------------------Variable Declaration---------------!
	!INTEGER VARIABLES
		!outType - Holds selector 0/1 for output data to screen/file
		!errors - Holds number of detected errors
	!REAL VARIABLES
		!flux - Holds the thermal neutron flux (#/s cm^2)
		!parentDensity - Holds the initial atomic density of parent species (#/cm^3)
		!crsAp - Holds the microscopic absorption cross section of parent species (b)
		!crsCp - Holds the microscopic capture cross section of parent species (b)
		!crsAd - Holds the microscopic absorption cross section of daughter species (b)
		!lambda - Holds the decay constant of daughter species (/s)
	!---------------End Variable Declaration--------------!
	
	!Check input values for errors
	!Check flux
	IF(flux<=0) THEN
		CALL THROWERROR(errors,"Error: Thermal neutron flux must be greater than 0",outType)
	END IF
	!Check density
	IF(parentDensity<=0) THEN
		CALL THROWERROR(errors,"Error: Atomic density of parent species must be greater than 0",outType)
	END IF
	!Check parent absorption crs
	IF(crsAp<=0) THEN
		CALL THROWERROR(errors,"Error: Microscopic absorption cross section of parent species must be greater than 0",outType)
	END IF
	!Check parent capture crs
	IF(crsCp<=0) THEN
		CALL THROWERROR(errors,"Error: Microscopic capture cross section of parent species must be greater than 0",outType)
	END IF
	!Ensure parent capture crs <= parent absorption crs
	IF(crsCp>crsAp) THEN
		CALL THROWERROR(errors,"Error: Parent species capture crs cannot be less than its absorption crs",outType)
	END IF
	!Check daughter absorption crs
	IF(crsAd<=0) THEN
		CALL THROWERROR(errors,"Error: Microscopic absorption cross section of daughter species must be greater than 0",outType)
	END IF
	!Check decay constant
	IF(lambda<=0) THEN
		CALL THROWERROR(errors,"Error: Decay constant of daughter species must be greater than 0",outType)
	END IF
	
	!If there were any errors, output the number of errors, otherwise continue
	IF(errors>0) THEN
		WRITE(*,'(A,I1)') "Total number of errors in inputs: ",errors
		IF(outType==1) WRITE(*,'(A,I1)')"Total number of errors in inputs: ",errors
		STOP ": Program execution terminated due to fatal errors."
	ELSE
		!Proper input values, notify then proceed to (possibly) infinite loop
		IF(outType==1) THEN
			WRITE(12,*) "Congratulations on entering proper input values. Code execution will now proceed."
		ELSE
			WRITE(*,*) "Congratulations on entering proper input values. Code execution will now proceed."
		END IF
	END IF
END SUBROUTINE VERIFYINPUT






!Loops through cases and calculates solutions, outputting them to the correct place
SUBROUTINE CalculateAndOutput(inType,outType,line,parentDensity,flux,crsAp,crsAd,crsCp,lambda)
	IMPLICIT NONE
	
	REAL,INTENT(IN)::parentDensity,flux,crsAp,crsAd,crsCp,lambda
	INTEGER,INTENT(IN)::inType,outType
	CHARACTER(86),INTENT(IN)::line
	
	CHARACTER(86)::lineBot="======================================================================================"
	REAL::tf,dt,timesteps
	INTEGER::ioin,efd
	
	!------------------Variable Declaration---------------!
	!CHARACTER VARIABLES
		!caseTitle - Holds name of the case to be executed
		!fileOut - Holds name of output file, ex: "output.txt"
		!fileIn - Holds name of input file, ex: "input.txt"
		!lineBot - Horizontal borders and dividers for output table
		!line - Horizontal borders and dividers for output table
	!INTEGER VARIABLES
		!inType -  Holds selector 0/1 for input data from screen/file
		!outType - Holds selector 0/1 for output data to screen/file
		!ioin - IOSTAT for input file
		!efd - Holds solution selection 0/1/2
	!REAL VARIABLES
		!flux - Holds the thermal neutron flux (#/s cm^2)
		!parentDensity - Holds the initial atomic density of parent species (#/cm^3)
		!crsAp - Holds the microscopic absorption cross section of parent species (b)
		!crsCp - Holds the microscopic capture cross section of parent species (b)
		!crsAd - Holds the microscopic absorption cross section of daughter species (b)
		!lambda - Holds the decay constant of daughter species (/s)
		!tf - holds final time (seconds)
		!dt - holds timestep difference (seconds)
		!timesteps - holds exact number of timesteps (tf/dt)
	!---------------End Variable Declaration--------------!
	
	
	DO WHILE(.TRUE.)
		!Ask for final time and time step, record respectively for screen input case
		IF(inType==0) THEN
			WRITE(*,*) "Note: Set final time or time step <= 0 to stop execution:"
			WRITE(*,*)"Enter final time (seconds): "
			READ(*,*) tf
			WRITE(*,*)"Enter time step (seconds): "
			READ(*,*) dt
		ELSE
			!For file input case,
			!Read in final time and dt, if IOSTAT!=0 then EOF was found, terminate program
			READ(11,*,IOSTAT=ioin)tf,dt
			IF(ioin.NE.0) THEN
				WRITE(*,*) "End of input file found, terminating program"
				IF(outType==1) WRITE(12,*)"End of input file found, terminating program"
				EXIT
			END IF
			!Read in solution style, if IOSTAT!=0 then EOF was found, terminate program
			READ(11,*,IOSTAT=ioin)efd
			IF(ioin.NE.0) THEN
				WRITE(*,'(A38)',ADVANCE="NO") "Input file ended earlier than expected"
				WRITE(*,'(A81)')"(final time and dt exist but no solution style on next line), terminating program"
				IF(outType==1) THEN
					WRITE(12,'(A38)',ADVANCE="NO")"Input file ended earlier than expected"
					WRITE(12,'(A81)')"(final time and dt exist but no solution style on next line), terminating program"
				END IF
				EXIT
			END IF
		END IF
		
		!if tf or dt are non-positive, notify/terminate
		IF((tf<=0).OR.(dt<=0)) THEN
			IF(inType==0) THEN
				WRITE(*,*) "Final time or dt entered as non-positive, Terminating execution of all selected cases"
			ELSE
				WRITE(12,*) "Final time or dt entered as non-positive, Terminating execution of all selected cases"
			END IF
			EXIT
		END IF
		
		!Define format for case headers and footers
		373 FORMAT (A35,A20,A35,/)
		
		!tf and dt are both positive, begin new case
		!For screen input, ask and record if Exact/FD
		IF(outType==1) THEN
			WRITE(12,373) line,"   BEGIN NEW CASE   ",line
		ELSE
			WRITE(*,373) line,"   BEGIN NEW CASE   ",line
		END IF
		IF(inType==0) THEN
			WRITE(*,*) "Enter 0/1/2 for Exact, explicit FD, or implicit FD solution:"
			READ(*,*) efd
		END IF
		
		!Calculate (REAL) number of timesteps
		timesteps=tf/dt
		
		SELECT CASE(efd)
			!For Exact Solution
			CASE(0)
				IF(outType==1) THEN
					WRITE(12,*) " =====  Exact Solution Case:"
				ELSE
					WRITE(*,*) " =====  Exact Solution Case:"
				END IF
				CALL DISPLAYCASETIMES(outType,tf,dt)
				CALL EXACT(outType,tf,dt,parentDensity,flux,crsAp,crsAd,crsCp,lambda)
			!For Explicit FD Solution
			CASE(1)
				IF(outType==1) THEN
					WRITE(12,*) " =====  Explicit Finite Difference Solution Case:"
				ELSE
					WRITE(*,*) " =====  Explicit Finite Difference Solution Case:"
				END IF
				CALL DISPLAYCASETIMES(outType,tf,dt)
				CALL EXPLICITFD(outType,tf,dt,parentDensity,flux,crsAp,crsAd,crsCp,lambda)
			!For Implicit FD Solution
			CASE(2)
				IF(outType==1) THEN
					WRITE(12,*) " =====  Implicit Finite Difference Solution Case:"
				ELSE
					WRITE(*,*) " =====  Implicit Finite Difference Solution Case:"
				END IF
				CALL DISPLAYCASETIMES(outType,tf,dt)
				CALL IMPLICITFD(outType,tf,dt,parentDensity,flux,crsAp,crsAd,crsCp,lambda)
			!Default case, incorrect value chosen for efd
			CASE DEFAULT
				IF(outType==1) THEN
					WRITE(12,'(A,I4,A,/)')"Error: solution type must be 0,1, or 2. Entered value: ",efd,". Continuing to next case"
					WRITE(12,373) lineBot,"====  END CASE  ====",lineBot
					CYCLE
				ELSE
					WRITE(*,'(A,I4,A,/)')"Error: solution type must be 0,1, or 2. Entered value: ",efd,". Continuing to next case"
					WRITE(*,373) lineBot,"====  END CASE  ====",lineBot
					CYCLE
				END IF
		END SELECT
		
		!Draw end of table to designated output
		IF(outType==1) THEN
			WRITE(12,373) lineBot,"====  END CASE  ====",lineBot
		ELSE
			WRITE(*,373) lineBot,"====  END CASE  ====",lineBot
		END IF
	END DO
END SUBROUTINE CalculateAndOutput



!Calculates exact solution of a case
SUBROUTINE EXACT(outType,tf,dt,No,flux,crsAp,crsAd,crsCp,lambda)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::outType
	REAL,INTENT(IN)::tf,dt,No,flux,crsAp,crsAd,crsCp,lambda
	
	REAL::timesteps,temp
	INTEGER::i
	REAL::e=2.7182818,cmPerb=1.0E-24
	REAL,ALLOCATABLE::points(:,:)
	
	!------------------Variable Declaration---------------!
	!INTEGER VARIABLES
		!outType - Holds selector 0/1 for output data to screen/file
		!i - index variable of iteration
	!REAL VARIABLES
		!flux - Holds the thermal neutron flux (#/s cm^2)
		!No - Holds the initial atomic density of parent species (#/cm^3)
		!crsAp - Holds the microscopic absorption cross section of parent species (b)
		!crsCp - Holds the microscopic capture cross section of parent species (b)
		!crsAd - Holds the microscopic absorption cross section of daughter species (b)
		!lambda - Holds the decay constant of daughter species (/s)
		!tf - holds final time (seconds)
		!dt - holds timestep difference (seconds)
		!timesteps - holds exact number of timesteps (tf/dt)
		!temp - temporary variable to make algebra easier to read/write
		!e - euler's number
		!cmPerb - cm per barn
		!points - (timestep,1/2/3) 1=time 2=Parent 3=Daughter
	!---------------End Variable Declaration--------------!
	
	!Set timesteps
	timesteps=tf/dt
	
	!Allocate solution datapoints array
	ALLOCATE(points(CEILING(timesteps)+1,3))
	
	!Define format for data rows
	200 FORMAT (T12,F10.4,T29,E13.6,T46,E13.6)
	
	!Loop to solve for and write body rows
	DO i=0,CEILING(timesteps)
		!Resolve points 1-3 as time, NP, ND - Utilizes temp when needed
		!Set temp as a simple time calculation
		temp = dt*REAL(i)
		!Set current time
		points(i+1,1) = temp
		!Calculate Densities at time points(i+1,1)
		points(i+1,2)=No*(e**(0-(flux*crsAp*cmPerb*points(i+1,1))))
		temp=lambda+((crsAd-crsAp)*flux*cmPerb)
		points(i+1,3)=No*crsCp*cmPerb*flux*(e**(0-(crsAp*cmPerb*points(i+1,1)*flux)))/temp
		points(i+1,3)=points(i+1,3)*(1-(e**(0-(temp*points(i+1,1)))))
		!Write solution row for either file or screen
		IF(outType==1) THEN
			WRITE(12,200) points(i+1,1),points(i+1,2),points(i+1,3)
		ELSE
			WRITE(*,200) points(i+1,1),points(i+1,2),points(i+1,3)
		END IF
	END DO	
END SUBROUTINE EXACT

!Calculates Explicit Finite Difference solutions
SUBROUTINE EXPLICITFD(outType,tf,dt,No,flux,crsAp,crsAd,crsCp,lambda)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::outType
	REAL,INTENT(IN)::tf,dt,No,flux,crsAp,crsAd,crsCp,lambda
	
	REAL::temp
	INTEGER::i,n,errs(2)
	REAL::tn,cmPerb=1.0E-24
	REAL,ALLOCATABLE::points(:,:)
	
	!------------------Variable Declaration---------------!
	!INTEGER VARIABLES
		!outType - Holds selector 0/1 for output data to screen/file
		!i - index variable of iteration
		!n - number of timesteps
		!errs(2) - parent,daughter errors found
	!REAL VARIABLES
		!flux - Holds the thermal neutron flux (#/s cm^2)
		!No - Holds the initial atomic density of parent species (#/cm^3)
		!crsAp - Holds the microscopic absorption cross section of parent species (b)
		!crsCp - Holds the microscopic capture cross section of parent species (b)
		!crsAd - Holds the microscopic absorption cross section of daughter species (b)
		!lambda - Holds the decay constant of daughter species (/s)
		!tf - holds final time (seconds)
		!dt - holds timestep difference (seconds)
		!temp - temporary variable to make algebra easier to read/write
		!cmPerb - cm per barn
		!points - (timestep,1/2/3) 1=time 2=Parent 3=Daughter
	!---------------End Variable Declaration--------------!
	
	!Define format for data rows
	201 FORMAT (T12,F10.4,T29,E13.6,T46,E13.6)
	
	!Find number of timesteps and allocate output array, initialize error values
	n=CEILING(tf/dt)
	ALLOCATE(points(n+1,2))
	errs(1)=0
	errs(2)=0
	
	DO i=0,n
		!Calculate time
		tn=REAL(i)*dt
		!For first case set N1 as initial density, N2 to 0
		IF(i==0) THEN
			points(i+1,1)=No
			points(i+1,2)=0
		!For all i>0, use equations provided
		ELSE
			points(i+1,1)=points(i,1)*(1-(dt*(flux*crsAp)*cmPerb))
			points(i+1,2)=points(i,2)+(dt*((crsCp*(cmPerb*flux)*points(i,1))-(points(i,2)*(lambda+(crsAd*(cmPerb*flux))))))
		END IF
		!Check if values are negative
		IF(points(i+1,1)<0) errs(1)=errs(1)+1
		IF(points(i+1,2)<0) errs(2)=errs(2)+1
		!Write to designated output
		IF(outType==1) THEN
			WRITE(12,201) tn,points(i+1,1),points(i+1,2)
		ELSE
			WRITE(*,201) tn,points(i+1,1),points(i+1,2)
		END IF
	END DO
	!If errors were detected, warn
	IF((errs(1)>0).OR.(errs(2)>0)) THEN
		IF(outType==1) THEN
			WRITE(12,'(A85)')"<<<<<<<<<<<<<<<<<<<< WARNING: NEGATIVE ATOMIC DENSITIES COMPUTED >>>>>>>>>>>>>>>>>>>>"
			WRITE(12,'(A6,I4,A41)')"<<<<<<",errs(1)," negative parent nuclide densities >>>>>>"
			WRITE(12,'(A6,I4,A43)')"<<<<<<",errs(2)," negative daughter nuclide densities >>>>>>"
		ELSE
			WRITE(*,'(A85)')"<<<<<<<<<<<<<<<<<<<< WARNING: NEGATIVE ATOMIC DENSITIES COMPUTED >>>>>>>>>>>>>>>>>>>>"
			WRITE(*,'(A6,I4,A41)')"<<<<<<",errs(1)," negative parent nuclide densities >>>>>>"
			WRITE(*,'(A6,I4,A43)')"<<<<<<",errs(2)," negative daughter nuclide densities >>>>>>"
		END IF
	END IF
END SUBROUTINE EXPLICITFD




!Calculates Implicit Finite Difference solutions
SUBROUTINE IMPLICITFD(outType,tf,dt,No,flux,crsAp,crsAd,crsCp,lambda)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::outType
	REAL,INTENT(IN)::tf,dt,No,flux,crsAp,crsAd,crsCp,lambda
	
	REAL::temp
	INTEGER::i,n,errs(2)
	REAL::tn,cmPerb=1.0E-24
	REAL,ALLOCATABLE::points(:,:)
	
	!------------------Variable Declaration---------------!
	!INTEGER VARIABLES
		!outType - Holds selector 0/1 for output data to screen/file
		!i - index variable of iteration
		!n - number of timesteps
		!errs(2) - parent,daughter errors found
	!REAL VARIABLES
		!flux - Holds the thermal neutron flux (#/s cm^2)
		!No - Holds the initial atomic density of parent species (#/cm^3)
		!crsAp - Holds the microscopic absorption cross section of parent species (b)
		!crsCp - Holds the microscopic capture cross section of parent species (b)
		!crsAd - Holds the microscopic absorption cross section of daughter species (b)
		!lambda - Holds the decay constant of daughter species (/s)
		!tf - holds final time (seconds)
		!dt - holds timestep difference (seconds)
		!temp - temporary variable to make algebra easier to read/write
		!cmPerb - cm per barn
		!points - (timestep,1/2/3) 1=time 2=Parent 3=Daughter
	!---------------End Variable Declaration--------------!
	
	!Define format for data rows
	201 FORMAT (T12,F10.4,T29,E13.6,T46,E13.6)
	
	!Find number of timesteps and allocate output array, initialize error values
	n=CEILING(tf/dt)
	ALLOCATE(points(n+1,2))
	errs(1)=0
	errs(2)=0
	
	DO i=0,n
		!Calculate time
		tn=REAL(i)*dt
		!For first case set N1 as initial density, N2 to 0
		IF(i==0) THEN
			points(i+1,1)=No
			points(i+1,2)=0
		!For all i>0, use equations provided
		ELSE
			points(i+1,1)=points(i,1)/(1+(dt*(flux*cmPerb)*crsAp))
			points(i+1,2)=(points(i,2)+(points(i,1)*dt*(flux*cmPerb)*crsCp))/(1+(dt*(lambda+((flux*cmPerb)*crsAd))))
		END IF
		!Check if values are negative
		IF(points(i+1,1)<0) errs(1)=errs(1)+1
		IF(points(i+1,2)<0) errs(2)=errs(2)+1
		!Write to designated output
		IF(outType==1) THEN
			WRITE(12,201) tn,points(i+1,1),points(i+1,2)
		ELSE
			WRITE(*,201) tn,points(i+1,1),points(i+1,2)
		END IF
	END DO
	!If errors were detected, warn
	IF((errs(1)>0).OR.(errs(2)>0)) THEN
		IF(outType==1) THEN
			WRITE(12,'(A85)')"<<<<<<<<<<<<<<<<<<<< WARNING: NEGATIVE ATOMIC DENSITIES COMPUTED >>>>>>>>>>>>>>>>>>>>"
			WRITE(12,'(A6,I4,A41)')"<<<<<<",errs(1)," negative parent nuclide densities >>>>>>"
			WRITE(12,'(A6,I4,A43)')"<<<<<<",errs(2)," negative daughter nuclide densities >>>>>>"
		ELSE
			WRITE(*,'(A85)')"<<<<<<<<<<<<<<<<<<<< WARNING: NEGATIVE ATOMIC DENSITIES COMPUTED >>>>>>>>>>>>>>>>>>>>"
			WRITE(*,'(A6,I4,A41)')"<<<<<<",errs(1)," negative parent nuclide densities >>>>>>"
			WRITE(*,'(A6,I4,A43)')"<<<<<<",errs(2)," negative daughter nuclide densities >>>>>>"
		END IF
	END IF
END SUBROUTINE IMPLICITFD




!Writes error message and increments the error integer up by 1
SUBROUTINE THROWERROR(nerr,errormessage,outType)
	IMPLICIT NONE
	INTEGER,INTENT(INOUT)::nerr	!number of errors
	INTEGER,INTENT(IN)::outType !Defined in main program
	CHARACTER,INTENT(IN)::errormessage	!Error message to output
	!Write error messsage to designated output (if file, also write to screen)
	IF(outType==1) WRITE(12,*)errormessage
	WRITE(*,'(A)')errormessage
	nerr=nerr+1!Increment number of errors
END SUBROUTINE THROWERROR




!Displays initial time values for the case
SUBROUTINE DISPLAYCASETIMES(outType,tf,dt)
	IMPLICIT NONE
	INTEGER,INTENT(IN)::outType !Defined in main program
	REAL,INTENT(IN)::tf,dt !final time (s),time step difference (s)
	
	999 FORMAT (8X,A,T26,F12.4)!define case time write format
	
	SELECT CASE(outType)
		!for Screen output
		CASE(0)
			WRITE(*,999) "Initial time =",0.0000
			WRITE(*,999) "Final time   =",tf
			WRITE(*,999) "Time Step    =",dt
			WRITE(*,'(/,T13,A,T30,A,T47,A)') "Time(sec)","Nparent(cm^-3)","Ndaughter(cm^-3)"!Write table column headers
		!for File output
		CASE(1)
			WRITE(12,999) "Initial time =",0.0000
			WRITE(12,999) "Final time   =",tf
			WRITE(12,999) "Time Step    =",dt
			WRITE(12,'(/,T13,A,T30,A,T47,A)') "Time(sec)","Nparent(cm^-3)","Ndaughter(cm^-3)"!Write table column headers
		CASE DEFAULT
			WRITE(*,*)"Error: no valid output type to display case times, this error should never occur"
			STOP
	END SELECT
END SUBROUTINE DISPLAYCASETIMES



!Concluding output(s) from the program
SUBROUTINE ProgramEnd(outType,fileOut)
	IMPLICIT NONE
	CHARACTER(72),INTENT(IN)::fileOut !output file name
	INTEGER,INTENT(IN)::outType !0/1 screen/file
	
	IF(outType==1) THEN
		WRITE(*,'(/,A68,A72)')"Program executed successfully, the output can be found in the file: ",fileOut
	ELSE
		WRITE(*,'(/,A52)')"Program executed successfully, terminating execution"
	END IF
END SUBROUTINE ProgramEnd